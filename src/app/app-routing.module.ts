import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { InscripcionDelegadoComponent } from './inscripcion_delegado/inscripcion_delegado.component';
import { TorneosComponent } from './torneos/torneos.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'inscripcion_delegado', component: InscripcionDelegadoComponent},
  {path: 'torneos', component: TorneosComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
