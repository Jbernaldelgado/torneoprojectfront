import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InscripcionDelegadoComponent } from './inscripcion_delegado/inscripcion_delegado.component';
import { TorneosComponent } from './torneos/torneos.component';
import { HttpClientModule } from '@angular/common/http'; 
import { FormsModule,ReactiveFormsModule }   from '@angular/forms';
import { DataViewModule } from 'primeng/dataview'
import { DialogModule } from 'primeng/dialog';
import { ButtonModule } from 'primeng/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastModule } from 'primeng/toast';
import { ConfirmationService, MessageService } from 'primeng/api';
import { PanelModule } from 'primeng/panel';
import { DropdownModule } from 'primeng/dropdown';
import {InputTextModule} from 'primeng/inputtext';
import {RatingModule} from 'primeng/rating';
import {RippleModule} from 'primeng/ripple';
import { TabViewModule } from 'primeng/tabview';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import {TableModule} from 'primeng/table';
import {TooltipModule} from 'primeng/tooltip';

@NgModule({
  declarations: [
    AppComponent,
    InscripcionDelegadoComponent,
    TorneosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    DataViewModule,
    DialogModule,
    ButtonModule,
    BrowserAnimationsModule,
    ToastModule,
    RippleModule,
    PanelModule,
    DropdownModule,
    InputTextModule,
    RatingModule,
    TabViewModule,
    ConfirmDialogModule,
    TableModule,
    TooltipModule
  ],
  providers: [MessageService, ConfirmationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
