import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ConfirmationService, Message, MessageService } from 'primeng/api';
import { ICampeonato } from 'src/models/campeonato.model';
import { IUsuario, Usuario } from 'src/models/usuario.model';
import { UsuarioService } from 'src/services/usuario.service';
import { commonMessages } from 'src/shared/constants/commonMessages';
import { IlistarCampeonatos, IlistarDelegados, IlistarEquipos, IOpcionVo } from 'src/shared/vo/opcion-vo';
import { CampeonatoService } from 'src/services/campeonato.service';
import { EquipoService } from 'src/services/equipo.service';
import { Equipo, IEquipo } from 'src/models/equipo.model';
import { Grupo } from 'src/models/grupo.model';
import { GrupoService } from 'src/services/grupo.service';

@Component({
  selector: 'inscripcion_delegado',
  templateUrl: './inscripcion_delegado.component.html',
  styleUrls: ['./inscripcion_delegado.component.scss']
})
export class InscripcionDelegadoComponent implements OnInit{
  formDatosDelegado!: FormGroup;
  formDatosDelegadoEditar!: FormGroup;
  formDatosEquipo!: FormGroup;
  labels = commonMessages;
  mensajeNombreDelegado: any;
  mensajeApellidoDelegado: any;
  mensajeUsuario: any;
  mensajeIdentificacion: any;
  mensajeCelular: any;
  mensajeContrasena: any;
  mensajeRepetirContrasena: any;
  mensajeCampeonato: any;
  mensajeNombreEquipo: any;
  mensajeDelegado: any;
  identificacionesValue: IOpcionVo[] = commonMessages.ARRAY_IDENTIFICACIONES;
  iconoOjoUno = "pi pi-eye";
  iconoOjoDos = "pi pi-eye";
  tipoContrasenaUno = "password";
  tipoContrasenaDos = "password";
  validacionIncorrecta = false;
  delegado!: Usuario;
  equipo!: Equipo;
  ConfirmarClave: any;
  listaUsuarios: Array<IUsuario> = [];
  listaDelegadosMostrar: Array<IlistarDelegados> = [];
  listaEquipos: Array<IEquipo> = [];
  listaEquiposMostrar: Array<IlistarEquipos> = [];
  roles: IOpcionVo[] = commonMessages.ARRAY_ROLES;
  displayModalDelegado: boolean;
  userDelegado: any;
  idTorneo = 0;
  listaCampeonatos: Array<ICampeonato> = [];
  listaCampeonatosMostrar: Array<IlistarCampeonatos> = [];

  constructor(
    private fb: FormBuilder,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private usuarioService: UsuarioService,
    private route: ActivatedRoute,
    private campeonatoService: CampeonatoService,
    private equipoService: EquipoService,
    private grupoService: GrupoService
  ) {
  }

  ngOnInit(): void {
    const param = this.route.snapshot.queryParamMap.get('torneo')!;
    this.idTorneo = parseInt(param, 10);
    this.cargarCampeonatos();
    this.cargarDelegados();
    this.cargarEquipos();
    this.crearFormularioDelegado();
    this.crearFormularioDelegadoEditar();
    this.crearFormularioEquipo();
  }

  showModalDialog(delegado:any) {
    this.userDelegado = delegado;
    this.cargarFormularioDelegado();
    this.displayModalDelegado = true;
  }

  hideModalDialog() {
    this.displayModalDelegado = false;
  }

  crearFormularioDelegado(): void {
    this.formDatosDelegado = this.fb.group({
      id: [''],
      usuario: ['',[Validators.required]],
      identificacion: ['',[Validators.required]],
      contrasena: ['',[Validators.required]],
      contrasenaRepetida: ['',[Validators.required]],
      idRol: ['',[Validators.required]],
      celular: ['',[Validators.required]],
      nombreDelegado: ['',[Validators.required]],
      apellidoDelegado: ['',[Validators.required]]
    });
  }

  crearFormularioDelegadoEditar(): void {
    this.formDatosDelegadoEditar = this.fb.group({
      id: [''],
      usuario: ['',[Validators.required]],
      identificacion: ['',[Validators.required]],
      contrasena: ['',[Validators.required]],
      contrasenaRepetida: ['',[Validators.required]],
      idRol: ['',[Validators.required]],
      celular: ['',[Validators.required]],
      nombreDelegado: ['',[Validators.required]],
      apellidoDelegado: ['',[Validators.required]]
    });
  }

  crearFormularioEquipo(): void {
    this.formDatosEquipo = this.fb.group({
      id: [''],
      nombreEquipo: ['',[Validators.required]],
      idCampeonato: ['',[Validators.required]],
      idDelegado: ['',[Validators.required]]
    });
  }

  cargarFormularioDelegado(): void {
    if(this.userDelegado){
      this.formDatosDelegadoEditar.patchValue({
        id: this.userDelegado.id,
        usuario: this.userDelegado.usuario,
        identificacion: this.userDelegado.identificacion,
        contrasena: this.userDelegado.contrasena,
        contrasenaRepetida: this.userDelegado.contrasena,
        idRol: "Delegado",
        celular: this.userDelegado.celular,
        nombreDelegado: this.userDelegado.nombre,
        apellidoDelegado: this.userDelegado.apellido
      });
    }
  }


  agregarDelegado(): void{
    this.validacionIncorrecta = false;
    this.mensajeNombreDelegado = "";
    this.mensajeApellidoDelegado = "";
    this.mensajeUsuario = "";
    this.mensajeIdentificacion = "";
    this.mensajeCelular = "";
    this.mensajeContrasena = "";
    this.mensajeRepetirContrasena = "";
    this.delegado = new Usuario();
    this.delegado.nombre = this.formDatosDelegado.controls['nombreDelegado'].value;
    this.delegado.apellido = this.formDatosDelegado.controls['apellidoDelegado'].value;
    this.delegado.usuario = this.formDatosDelegado.controls['usuario'].value;
    this.delegado.identificacion = this.formDatosDelegado.controls['identificacion'].value;
    this.delegado.idRol = 2;
    this.delegado.celular = this.formDatosDelegado.controls['celular'].value;
    this.delegado.contrasena = this.formDatosDelegado.controls['contrasena'].value;
    this.ConfirmarClave = this.formDatosDelegado.controls['contrasenaRepetida'].value;
    if(!this.delegado.nombre){
      this.mensajeNombreDelegado = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!this.delegado.apellido){
      this.mensajeApellidoDelegado = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!this.delegado.usuario){
      this.mensajeUsuario = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!this.delegado.identificacion){
      this.mensajeIdentificacion = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!this.delegado.celular){
      this.mensajeCelular = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!this.delegado.contrasena){
      this.mensajeContrasena = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!this.ConfirmarClave){
      this.mensajeRepetirContrasena = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }else{
      if(this.ConfirmarClave !== this.delegado.contrasena){
        this.mensajeRepetirContrasena = this.labels.CONTRASENA_NO_COINCIDEN;
        this.validacionIncorrecta = true;
      }
    }
    if (this.validacionIncorrecta === false) {
      this.confirmationService.confirm({
        message: 'Seguro desea agregar el delegado?',
        header: 'Confirmar',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
          this.usuarioService.create(this.delegado).subscribe(
            data => {
              if (data!== null) {
                this.showSuccess();
                //this.listaCampeonatosMostrar = [];
                //this.cargarCampeonatos();
                this.limpiarCampos();
              }
            },
            err => {
              this.showError();
            }
          );
        },
        reject: () => {
        }
      });
    }
  }

  editarDelegado(): void{
    this.validacionIncorrecta = false;
    this.mensajeNombreDelegado = "";
    this.mensajeApellidoDelegado = "";
    this.mensajeUsuario = "";
    this.mensajeIdentificacion = "";
    this.mensajeCelular = "";
    this.mensajeContrasena = "";
    this.mensajeRepetirContrasena = "";
    this.delegado = new Usuario();
    this.delegado.id = this.userDelegado.id;
    this.delegado.nombre = this.formDatosDelegadoEditar.controls['nombreDelegado'].value;
    this.delegado.apellido = this.formDatosDelegadoEditar.controls['apellidoDelegado'].value;
    this.delegado.usuario = this.formDatosDelegadoEditar.controls['usuario'].value;
    this.delegado.identificacion = this.formDatosDelegadoEditar.controls['identificacion'].value;
    this.delegado.idRol = 2;
    this.delegado.celular = this.formDatosDelegadoEditar.controls['celular'].value;
    this.delegado.contrasena = this.formDatosDelegadoEditar.controls['contrasena'].value;
    this.ConfirmarClave = this.formDatosDelegadoEditar.controls['contrasenaRepetida'].value;
    if(!this.delegado.nombre){
      this.mensajeNombreDelegado = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!this.delegado.apellido){
      this.mensajeApellidoDelegado = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!this.delegado.usuario){
      this.mensajeUsuario = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!this.delegado.identificacion){
      this.mensajeIdentificacion = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!this.delegado.celular){
      this.mensajeCelular = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!this.delegado.contrasena){
      this.mensajeContrasena = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!this.ConfirmarClave){
      this.mensajeRepetirContrasena = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }else{
      if(this.ConfirmarClave !== this.delegado.contrasena){
        this.mensajeRepetirContrasena = this.labels.CONTRASENA_NO_COINCIDEN;
        this.validacionIncorrecta = true;
      }
    }
    if (this.validacionIncorrecta === false) {
      this.confirmationService.confirm({
        message: 'Seguro desea editar el delegado?',
        header: 'Confirmar',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
          this.usuarioService.update(this.delegado).subscribe(
            data => {
              if (data!== null) {
                this.listaDelegadosMostrar = [];
                this.cargarDelegados();
                this.crearFormularioDelegadoEditar();
                this.hideModalDialog();
                this.showSuccessEdit();
                this.limpiarCamposEditar();
              }
            },
            err => {
              this.showErrorEdit();
            }
          );
        },
        reject: () => {
        }
      });
    }
  }

  showSuccess() {
    this.messageService.add({key: 'c',severity:'success', summary: 'Exitoso', detail: 'Delegado agregado correctamente'});
  }

  showSuccessEdit() {
    this.messageService.add({key: 'c',severity:'success', summary: 'Exitoso', detail: 'Delegado editado correctamente'});
  }

  showSuccessDelete() {
    this.messageService.add({key: 'c',severity:'success', summary: 'Exitoso', detail: 'Delegado eliminado correctamente'});
  }

  showSuccessEquipo() {
    this.messageService.add({key: 'c',severity:'success', summary: 'Exitoso', detail: 'Equipo agregado correctamente'});
  }

  showError() {
    this.messageService.add({severity:'error', summary: 'Error', detail: 'No se pudo agregar el delegado'});
  }

  showErrorEdit() {
    this.messageService.add({severity:'error', summary: 'Error', detail: 'No se pudo editar el delegado'});
  }

  showErrorDelete() {
    this.messageService.add({severity:'error', summary: 'Error', detail: 'No se pudo editar el delegado'});
  }

  showErrorEquipo() {
    this.messageService.add({severity:'error', summary: 'Error', detail: 'No se pudo agregar el equipo'});
  }

  primeraContrasena(): void{
    if(this.iconoOjoUno === "pi pi-eye"){
      this.iconoOjoUno = "pi pi-eye-slash";
      this.tipoContrasenaUno = "text";
    }else{
      this.iconoOjoUno = "pi pi-eye";
      this.tipoContrasenaUno = "password";
    }
  }

  segundaContrasena(): void{
    if(this.iconoOjoDos === "pi pi-eye"){
      this.iconoOjoDos = "pi pi-eye-slash";
      this.tipoContrasenaDos = "text";
    }else{
      this.iconoOjoDos = "pi pi-eye";
      this.tipoContrasenaDos = "password";
    }
  }

  limpiarCampos():void{
    this.formDatosDelegado.reset();
  }

  limpiarCamposEditar():void{
    this.formDatosDelegado.reset();
  }

  limpiarCamposEquipo():void{
    this.formDatosEquipo.reset();
  }

  cargarDelegados():void{
    this.usuarioService.getDelegado(2).subscribe( delegados => {
      this.listaUsuarios = delegados;
      this.listaUsuarios.forEach(element => {
        const rol = this.roles.find(rol => rol.codigo === element.idRol);
        this.listaDelegadosMostrar.push({
          id: element.id.toString(),
          usuario: element.usuario,
          identificacion: element.identificacion,
          rol: rol.nombre,
          celular: element.celular.toString(),
          nombre: element.nombre,
          apellido: element.apellido,
          contrasena: element.contrasena
        });
      });
    });
  }

  eliminarDelegado(delegado: any): void{
    this.confirmationService.confirm({
      message: 'Seguro desea eliminar el delegado, con toda la información relacionada?',
      header: 'Confirmar',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.usuarioService.delete(delegado.id).subscribe(
          data => {
            this.showSuccessDelete();
          },
          err => {
            this.showErrorDelete();
          }
        );
      },
      reject: () => {
      }
    });
  }

  cargarCampeonatos(): void{
    this.campeonatoService.getCampeonatos().subscribe(campeonatos =>{
      this.listaCampeonatos = campeonatos;
      this.listaCampeonatos.forEach(element => {
        this.listaCampeonatosMostrar.push({
          id: element.id.toString(),
          nombre_campeonato: element.nombreCampeonato
        });
      });
    });
  }

  cargarEquipos(): void{
    this.equipoService.getEquipos().subscribe(equipos =>{
      this.listaEquipos = equipos;
      this.listaEquipos.forEach(element => {
        this.listaEquiposMostrar.push({
          id: element.id.toString(),
          nombre: element.nombre,
          campeonato: element.idCampeonato.nombreCampeonato,
          delegado: element.delegado.nombre
        });
      });
    });
  }

  agregarEquipo(): void {
    this.validacionIncorrecta = false;
    this.mensajeNombreEquipo = "";
    this.mensajeCampeonato = "";
    this.mensajeDelegado = "";
    this.equipo = new Equipo();
    this.equipo.nombre = this.formDatosEquipo.controls['nombreEquipo'].value;
    const campeonato = this.formDatosEquipo.controls['idCampeonato'].value;
    const delegado = this.formDatosEquipo.controls['idDelegado'].value;
    if(!this.equipo.nombre){
      this.mensajeNombreEquipo = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!campeonato){
      this.mensajeCampeonato = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!delegado){
      this.mensajeDelegado = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if (this.validacionIncorrecta === false) {
      this.usuarioService.getUsuario(delegado).subscribe(usuario => {
        console.log("usuario:");
        console.log(usuario);
        this.equipo.delegado = usuario;
        this.campeonatoService.getCampeonato(campeonato).subscribe(campeonato => {
          this.equipo.idCampeonato = campeonato;
          this.equipo.estado = "A";
          this.equipo.puntos = 0;
          this.grupoService.getgrupo(1).subscribe(grupo => {
            this.equipo.idGrupo = grupo;
            console.log("equipo:");
            console.log(this.equipo);
            this.confirmationService.confirm({
              message: 'Seguro desea agregar el equipo?',
              header: 'Confirmar',
              icon: 'pi pi-exclamation-triangle',
              accept: () => {
                this.equipoService.create(this.equipo).subscribe(
                  data => {
                    if (data!== null) {
                      this.showSuccessEquipo();
                      this.limpiarCamposEquipo();
                    }
                  },
                  err => {
                    this.showErrorEquipo();
                  }
                );
              },
              reject: () => {
              }
            });
          });
        });
      });
    }
  }
}