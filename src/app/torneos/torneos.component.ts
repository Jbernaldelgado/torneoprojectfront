import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Campeonato, ICampeonato } from 'src/models/campeonato.model';
import { CampeonatoService } from 'src/services/campeonato.service';
import { commonMessages } from 'src/shared/constants/commonMessages';
import { IlistarCampeonatos, IOpcionVo } from 'src/shared/vo/opcion-vo';
import { MessageService} from 'primeng/api';
import { Router } from '@angular/router';

@Component({
  selector: 'torneos',
  templateUrl: './torneos.component.html',
  styleUrls: ['./torneos.component.scss']
})
export class TorneosComponent implements OnInit{
  displayModal: boolean;
  formDatosCampeonato!: FormGroup;
  listaCampeonatos: Array<ICampeonato> = [];
  listaCampeonatosMostrar: Array<IlistarCampeonatos> = [];
  deportes: IOpcionVo[] = commonMessages.ARRAY_DEPORTES;
  categorias: IOpcionVo[] = commonMessages.ARRAY_CATEGORIAS;
  localidades: IOpcionVo[] = commonMessages.ARRAY_LOCALIDADES;
  rango_annios: IOpcionVo[] = commonMessages.ARRAY_ANNIOS;
  planillas: IOpcionVo[] = commonMessages.ARRAY_PLANILLAS;
  divisiones: IOpcionVo[] = commonMessages.ARRAY_DIVISIONES;
  rutaImagenFutbol = "assets/img/futbol.png";
  rutaImagenBasket = 'assets/img/basket.png';
  labels = commonMessages;
  deportesValue: IOpcionVo[] = commonMessages.ARRAY_DEPORTES;
  categoriasValue: IOpcionVo[] = commonMessages.ARRAY_CATEGORIAS;
  ramasValue: IOpcionVo[] = commonMessages.ARRAY_RAMAS;
  localidadesValue: IOpcionVo[] = commonMessages.ARRAY_LOCALIDADES;
  rangoAnniosValue: IOpcionVo[] = commonMessages.ARRAY_ANNIOS;
  planillasValue: IOpcionVo[] = commonMessages.ARRAY_PLANILLAS;
  divisionesValue: IOpcionVo[] = commonMessages.ARRAY_DIVISIONES;
  campeonato!: Campeonato;
  closeModal: string
  cargoDatos = false;
  validacionIncorrecta = false;
  mensajeNombre : any;
  mensajeDeporte : any;
  mensajeCategoria : any;
  mensajeRama : any;
  mensajeLocalidad : any;
  mensajeRangoAnnio : any;
  mensajePlanilla : any;
  mensajeDivision: any;

  constructor(
    private campeonatoService: CampeonatoService,
    private fb: FormBuilder,
    private messageService: MessageService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.cargarCampeonatos();
    this.crearFormularioCampeonato();
  }

  showModalDialog() {
    this.displayModal = true;
  }

  hideModalDialog() {
    this.displayModal = false;
  }

  showSuccess() {
    this.messageService.add({key: 'c',severity:'success', summary: 'Exitoso', detail: 'Campeonato agregado correctamente'});
  }

  showError() {
    this.messageService.add({severity:'error', summary: 'Error', detail: 'No se pudo agregar el campeonato'});
  }

  crearFormularioCampeonato(): void {
    this.formDatosCampeonato = this.fb.group({
      id: [''],
      nombreCampeonato: ['',[Validators.required]],
      deporte: ['',[Validators.required]],
      categoria: ['',[Validators.required]],
      rama: ['',[Validators.required]],
      localidad: ['',[Validators.required]],
      rangoAnnio: ['',[Validators.required]],
      planilla: ['',[Validators.required]],
      division: ['',[Validators.required]]
    });
  }

  cargarCampeonatos(): void {
    this.campeonatoService.getCampeonatos().subscribe( campeonatos => {
      this.listaCampeonatos = campeonatos;
      this.listaCampeonatos.forEach(element => {
        const deporte = this.deportes.find(deporte => deporte.codigo === element.deporte);
        const categoria = this.categorias.find(categoria => categoria.codigo === element.categoria);
        const localidad = this.localidades.find(localidad => localidad.codigo === element.localidad);
        const rango_annio = this.rango_annios.find(rango_annio => rango_annio.codigo === element.rangoAnnio);
        const planilla = this.planillas.find(planilla => planilla.codigo === element.planilla);
        const division = this.divisiones.find(division => division.codigo === element.division);
        this.listaCampeonatosMostrar.push({
          id: element.id.toString(),
          nombre_campeonato: element.nombreCampeonato,
          estado: element.estado,
          deporte: deporte.nombre,
          categoria: categoria.nombre,
          rama: element.rama,
          localidad: localidad.nombre,
          rango_annio: rango_annio.nombre,
          planilla: planilla.nombre,
          division: division.nombre
        });
      });
    });
    this.cargoDatos = true;
  }

  agregarCampeonato(): void {
    this.validacionIncorrecta = false;
    this.mensajeNombre = "";
    this.mensajeDeporte = "";
    this.mensajeCategoria = "";
    this.mensajeRama = "";
    this.mensajeLocalidad = "";
    this.mensajeRangoAnnio = "";
    this.mensajePlanilla = "";
    this.mensajeDivision = "";
    this.campeonato = new Campeonato();
    this.campeonato.nombreCampeonato = this.formDatosCampeonato.controls['nombreCampeonato'].value;
    this.campeonato.estado = "ACTIVO";
    this.campeonato.deporte = this.formDatosCampeonato.controls['deporte'].value;
    this.campeonato.categoria = this.formDatosCampeonato.controls['categoria'].value;
    this.campeonato.rama = this.formDatosCampeonato.controls['rama'].value;
    this.campeonato.localidad = this.formDatosCampeonato.controls['localidad'].value;
    this.campeonato.rangoAnnio = this.formDatosCampeonato.controls['rangoAnnio'].value;
    this.campeonato.planilla = this.formDatosCampeonato.controls['planilla'].value;
    this.campeonato.division = this.formDatosCampeonato.controls['division'].value;
    if(!this.campeonato.nombreCampeonato){
      this.mensajeNombre = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!this.campeonato.deporte){
      this.mensajeDeporte = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!this.campeonato.categoria){
      this.mensajeCategoria = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!this.campeonato.rama){
      this.mensajeRama = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!this.campeonato.localidad){
      this.mensajeLocalidad = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!this.campeonato.rangoAnnio){
      this.mensajeRangoAnnio = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!this.campeonato.planilla){
      this.mensajePlanilla = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if(!this.campeonato.division){
      this.mensajeDivision = this.labels.CAMPO_OBLIGATORIO_LABEL;
      this.validacionIncorrecta = true;
    }
    if (this.validacionIncorrecta === false) {
      this.campeonatoService.create(this.campeonato).subscribe(
        data => {
          if (data!== null) {
            this.showSuccess();
            this.hideModalDialog();
            this.listaCampeonatosMostrar = [];
            this.cargarCampeonatos();
            this.limpiarCampos();
          }
        },
        err => {
          this.showError();
        }
      );
    }
  }

  limpiarCampos():void{
    this.formDatosCampeonato.reset();
  }

  verCampeonato(item:any):void{
    this.router.navigate(['inscripcion_delegado'], { queryParams: { torneo: item.id } });
  }

}