import { Moment } from 'moment';
import { IEquipo } from './equipo.model';

export interface IDeportista {
    id?: number;
    identificacion?: string;
    tipoIdentificacion?: number;
    nombre?: string;
    apellido?: string;
    fechaNacimiento?: Moment;
    carnet?: string;
    fotoDeportista?: string;
    numeroCamiseta?: number;
    idEquipo?: IEquipo;
    posicion?: string;
    golesRecibidos?: number;
    anotaciones?: number;
  }
  
export class Deportista implements IDeportista {
    constructor(
        public id?: number,
        public identificacion?: string,
        public tipoIdentificacion?: number,
        public nombre?: string,
        public apellido?: string,
        public fechaNacimiento?: Moment,
        public carnet?: string,
        public fotoDeportista?: string,
        public numeroCamiseta?: number,
        public idEquipo?: IEquipo,
        public posicion?: string,
        public golesRecibidos?: number,
        public anotaciones?: number
    ) {}
}