
export interface IUsuario {
  id?: number;
  usuario?: string;
  identificacion?: string;
  contrasena?: string;
  idRol?: number;
  celular?: number;
  nombre?: string;
  apellido?: string;
}

export class Usuario implements IUsuario {
  constructor(
    public id?: number,
    public usuario?: string,
    public identificacion?: string,
    public contrasena?: string,
    public idRol?: number,
    public celular?: number,
    public nombre?: string,
    public apellido?: string
  ) {}
}