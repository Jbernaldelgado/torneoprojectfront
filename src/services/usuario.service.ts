import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { IUsuario } from 'src/models/usuario.model';
import { BackendURL } from 'src/shared/constants/app.constants';
import { createRequestOption } from 'src/shared/util/request-util';


type EntityResponseType = HttpResponse<IUsuario>;
type EntityArrayResponseType = HttpResponse<IUsuario[]>;

@Injectable({ providedIn: 'root' })
export class UsuarioService {
  public resourceUrl = BackendURL + 'api/usuarios';

  constructor(private http: HttpClient) {}

  public create(usuario: IUsuario): Observable<any> {
    return this.http.post(this.resourceUrl, usuario);
  }

  public update(usuario: IUsuario): Observable<any> {
    return this.http.put(this.resourceUrl, usuario);
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(`${this.resourceUrl}/${id}`);
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IUsuario>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IUsuario[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  getDelegado(delegado: any): Observable<any> {
    const url = this.resourceUrl + '/listarDelegados?delegado=' + delegado;
    return this.http.get<any>(url);
  }

  getUsuario(usuario: any): Observable<any> {
    const url = this.resourceUrl + '/listarUsuario?usuario=' + usuario;
    return this.http.get<any>(url);
  }
}