export const commonMessages = {
    ARRAY_DEPORTES: [
        { codigo: 1, nombre: 'Futbol' },
        { codigo: 2, nombre: 'Baloncesto' },
        { codigo: 3, nombre: 'Futsala' }
    ],
    ARRAY_CATEGORIAS: [
        { codigo: 1, nombre: 'Infantil' },
        { codigo: 2, nombre: 'Junior' },
        { codigo: 3, nombre: 'Juvenil' },
        { codigo: 4, nombre: 'Mayores' },
        { codigo: 5, nombre: 'Senior-Master' }
    ],
    ARRAY_LOCALIDADES: [
        { codigo: 1, nombre: 'Cancha divino niño' },
        { codigo: 2, nombre: 'Estadio municipal' },
        { codigo: 3, nombre: 'Coliseo Ciudad jardin' }
    ],
    ARRAY_ANNIOS: [
        { codigo: 1, nombre: 'Desde 10 a 15 años' },
        { codigo: 2, nombre: 'Desde 15 a 30 años' },
        { codigo: 3, nombre: 'Libre' }
    ],
    ARRAY_PLANILLAS: [
        { codigo: 1, nombre: 'Planilla Futbol' },
        { codigo: 2, nombre: 'Planilla Baloncesto' },
        { codigo: 3, nombre: 'Planilla Futsala' }
    ],
    ARRAY_RAMAS:[
        { codigo: 'M', nombre: 'Masculina' },
        { codigo: 'F', nombre: 'Femenina' },
    ],
    ARRAY_DIVISIONES:[
        { codigo: 1, nombre: 'Aficionada' },
        { codigo: 2, nombre: 'Profesional' },
    ],
    ARRAY_IDENTIFICACIONES:[
        { codigo: 1, nombre: 'Tarjeta de Identidad' },
        { codigo: 2, nombre: 'Cedula' },
    ],
    ARRAY_ROLES:[
        { codigo: 1, nombre: 'Administrador' },
        { codigo: 2, nombre: 'Delegado' },
        { codigo: 3, nombre: 'Planillero' }
    ],
    NOMBRE_CAMPEONATO: "Nombre Campeonato",
    CAMPO_OBLIGATORIO_LABEL: 'El campo es requerido',
    DEPORTE: "Deporte",
    SELECCIONE_OPCION: "Seleccione una opcion",
    CATEGORIA: "Categoria",
    RAMA: "Rama",
    LOCALIDAD: "Localidad",
    RANGO_ANNIO: "Rango en años",
    PLANILLA: "Planilla",
    HTTP_SUCCESS_LABEL: 'Datos guardados correctamente',
    HTTP_ERROR_LABEL: 'Error al guardar datos',
    DIVISION: "Division",
    NOMBRE_DELEGADO: "Nombre delegado",
    APELLIDO_DELEGADO: "Apellido delegado",
    USUARIO: "Usuario",
    IDENTIFICACION: "Identificación",
    ROL: "Rol",
    CELULAR: "Celular",
    CONTRASENA: "Contraseña",
    REPETIR_CONTRASENA: "Repetir contraseña",
    CONTRASENA_NO_COINCIDEN: 'Las contraseñas no coinciden',
    NOMBRE_EQUIPO:"Nombre del equipo",
    CAMPEONATO:"Campeonato",
    DELEGADO:"Delegado encargado"
}