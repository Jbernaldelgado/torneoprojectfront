export interface IOpcionVo {
    codigo: string | number | undefined;
    nombre: string;
}

export interface IlistarCampeonatos {
    id?: string | undefined;
    nombre_campeonato?: string | undefined;
    estado?: string | undefined;
    deporte?: string | undefined;
    categoria?: string | undefined;
    rama?: string | undefined;
    localidad?: string | undefined;
    rango_annio?: string | undefined;
    planilla?: string | undefined;
    division?: string | undefined;
}

export interface IlistarDelegados {
    id?: string | undefined;
    usuario?: string | undefined;
    identificacion?: string | undefined;
    rol?: string | undefined;
    celular?: string | undefined;
    nombre?: string | undefined;
    apellido?: string | undefined;
    contrasena?: string | undefined;
}

export interface IlistarEquipos {
    id?: string | undefined;
    nombre?: string | undefined;
    campeonato?: string | undefined;
    delegado?: string | undefined;
    grupo?: string | undefined;
    puntos?: string | undefined;
    estado?: string | undefined;
}